
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:12PM

See merge request itentialopensource/adapters/adapter-paloalto_prismacloud!14

---

## 0.4.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paloalto_prismacloud!12

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:21PM

See merge request itentialopensource/adapters/adapter-paloalto_prismacloud!11

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:34PM

See merge request itentialopensource/adapters/adapter-paloalto_prismacloud!10

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!9

---

## 0.3.5 [03-28-2024]

* Changes made at 2024.03.28_13:16PM

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!7

---

## 0.3.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_13:04PM

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:32PM

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:44AM

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!3

---

## 0.3.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!2

---

## 0.2.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!1

---

## 0.1.1 [01-18-2022]

- Initial Commit

See commit 692ec7c

---
