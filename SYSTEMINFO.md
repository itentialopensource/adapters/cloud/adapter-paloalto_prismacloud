# Prisma Cloud

Vendor: Paolo Alto Networks
Homepage: https://www.paloaltonetworks.com

Product: Prisma Cloud
Product Page: https://www.paloaltonetworks.com/prisma/cloud 

## Introduction
We classify Prisma Cloud into the Cloud domain as Prisma Cloud provides a solution for cloud security platform that provides visibility, threat detection, and compliance management for cloud-native applications and infrastructure.

## Why Integrate
The Prisma Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Prisma Cloud. With this adapter you have the ability to perform operations on items such as:

- Policy
- Search
- Alerts

## Additional Product Documentation
[Prisma Cloud API Documentation](https://pan.dev/prisma-cloud/api/cspm/)