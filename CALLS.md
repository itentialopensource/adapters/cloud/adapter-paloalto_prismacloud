## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Prisma Cloud. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Prisma Cloud.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Prismacloud. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">loginGenerateToken(body, callback)</td>
    <td style="padding:15px">Login Generate Token</td>
    <td style="padding:15px">{base_path}/{version}/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshToken(callback)</td>
    <td style="padding:15px">Refresh Token</td>
    <td style="padding:15px">{base_path}/{version}/auth_token/extend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSSOBypassAllowedUsers(callback)</td>
    <td style="padding:15px">Update SSO Bypass Allowed Users</td>
    <td style="padding:15px">{base_path}/{version}/auth_token/extend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">profile(callback)</td>
    <td style="padding:15px">Profile</td>
    <td style="padding:15px">{base_path}/{version}/user/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProfile(body, callback)</td>
    <td style="padding:15px">Update Profile</td>
    <td style="padding:15px">{base_path}/{version}/user/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsers(callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNewUserProfilev2(body, callback)</td>
    <td style="padding:15px">Add New User Profile v2</td>
    <td style="padding:15px">{base_path}/{version}/v2/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsernames(callback)</td>
    <td style="padding:15px">List Usernames</td>
    <td style="padding:15px">{base_path}/{version}/user/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserRoleType(callback)</td>
    <td style="padding:15px">List User Role Type</td>
    <td style="padding:15px">{base_path}/{version}/user/role/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersProfileOtherthanyourown(emailId, callback)</td>
    <td style="padding:15px">Get Users Profile (other than your own)</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserProfileOtherthanyourown(emailId, callback)</td>
    <td style="padding:15px">Update User Profile (other than your own)</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserProfile(callback)</td>
    <td style="padding:15px">Delete User Profile</td>
    <td style="padding:15px">{base_path}/{version}/user/USER_ID_HERE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserStatus(emailId, callback)</td>
    <td style="padding:15px">Update User Status</td>
    <td style="padding:15px">{base_path}/{version}/user/{pathv1}/status/TRUE_FALSE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEmailDomains(callback)</td>
    <td style="padding:15px">List Email Domains</td>
    <td style="padding:15px">{base_path}/{version}/user/domain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSSOBypassAllowedUsers(callback)</td>
    <td style="padding:15px">List SSO Bypass Allowed Users</td>
    <td style="padding:15px">{base_path}/{version}/user/saml/bypass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserRoles(callback)</td>
    <td style="padding:15px">List User Roles</td>
    <td style="padding:15px">{base_path}/{version}/user/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserRole(body, callback)</td>
    <td style="padding:15px">Add User Role</td>
    <td style="padding:15px">{base_path}/{version}/user/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserRoleNames(callback)</td>
    <td style="padding:15px">List User Role Names</td>
    <td style="padding:15px">{base_path}/{version}/user/role/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userRoleInfo(callback)</td>
    <td style="padding:15px">User Role Info</td>
    <td style="padding:15px">{base_path}/{version}/user/role/ROLE_ID_HERE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserRole(body, callback)</td>
    <td style="padding:15px">Update User Role</td>
    <td style="padding:15px">{base_path}/{version}/user/role/ROLE_ID_HERE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserRole(callback)</td>
    <td style="padding:15px">Delete User Role</td>
    <td style="padding:15px">{base_path}/{version}/user/role/ROLE_ID_HERE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessKeys(callback)</td>
    <td style="padding:15px">List Access Keys</td>
    <td style="padding:15px">{base_path}/{version}/access_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccessKeysCopy(body, callback)</td>
    <td style="padding:15px">List Access Keys Copy</td>
    <td style="padding:15px">{base_path}/{version}/access_keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessKeybyID(callback)</td>
    <td style="padding:15px">Get Access Key by ID</td>
    <td style="padding:15px">{base_path}/{version}/access_keys/ACCESS_KEY_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessKey(body, callback)</td>
    <td style="padding:15px">Update Access Key</td>
    <td style="padding:15px">{base_path}/{version}/access_keys/ACCESS_KEY_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessKey(callback)</td>
    <td style="padding:15px">Delete Access Key</td>
    <td style="padding:15px">{base_path}/{version}/access_keys/ACCESS_KEY_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessKeyStatus(callback)</td>
    <td style="padding:15px">Update Access Key Status</td>
    <td style="padding:15px">{base_path}/{version}/access_keys/ACCESS_KEY_ID/status/TRUE_OR_FALSE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudAccounts(callback)</td>
    <td style="padding:15px">List Cloud Accounts</td>
    <td style="padding:15px">{base_path}/{version}/cloud?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudAccountNames(onlyActive, accountGroupIds, cloudType, callback)</td>
    <td style="padding:15px">List Cloud Account Names</td>
    <td style="padding:15px">{base_path}/{version}/cloud/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudOrgAccounts(callback)</td>
    <td style="padding:15px">List Cloud Org Accounts</td>
    <td style="padding:15px">{base_path}/{version}/cloud/CLOUD_TYPE/CLOUD_ACCOUNT_ID/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudTypes(callback)</td>
    <td style="padding:15px">List Cloud Types</td>
    <td style="padding:15px">{base_path}/{version}/cloud/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountAWS(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (AWS)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/aws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountAzure(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (Azure)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/azure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountGCP(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (GCP)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/gcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountAliCloud(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (AliCloud)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/alibaba_cloud?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudAccountInfo(callback)</td>
    <td style="padding:15px">Cloud Account Info</td>
    <td style="padding:15px">{base_path}/{version}/cloud/CLOUD_TYPE/CLOUD_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudAccountAWS(skipStatusChecks, body, callback)</td>
    <td style="padding:15px">Update Cloud Account (AWS)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/CLOUD_TYPE/CLOUD_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountAzure1(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (Azure)1</td>
    <td style="padding:15px">{base_path}/{version}/cloud/azure/CLOUD_ACCOUNT_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountGCP1(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (GCP)1</td>
    <td style="padding:15px">{base_path}/{version}/cloud/gcp/CLOUD_ACCOUNT_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cAUTIONDELETECloudAccount(callback)</td>
    <td style="padding:15px">CAUTION - DELETE Cloud Account</td>
    <td style="padding:15px">{base_path}/{version}/cloud/gcp/CLOUD_ACCOUNT_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudAccountAliCloud1(body, callback)</td>
    <td style="padding:15px">Add Cloud Account (AliCloud)1</td>
    <td style="padding:15px">{base_path}/{version}/cloud/alibaba_cloud/CLOUD_ACCOUNT_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCloudAccount(body, callback)</td>
    <td style="padding:15px">Patch Cloud Account</td>
    <td style="padding:15px">{base_path}/{version}/CLOUD_TYPE/CLOUD_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAccountStatusAWS(body, callback)</td>
    <td style="padding:15px">Get Cloud Account Status (AWS)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/status/aws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAccountStatusAzure(body, callback)</td>
    <td style="padding:15px">Get Cloud Account Status (Azure)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/status/azure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAccountStatusGCP(body, callback)</td>
    <td style="padding:15px">Get Cloud Account Status (GCP)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/status/gcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudAccountStatusAliCloud(body, callback)</td>
    <td style="padding:15px">Get Cloud Account Status (AliCloud)</td>
    <td style="padding:15px">{base_path}/{version}/cloud/status/alibaba_cloud?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudAccountStatus(updateChildren, body, callback)</td>
    <td style="padding:15px">Update Cloud Account Status</td>
    <td style="padding:15px">{base_path}/{version}/cloud/ACCOUNT_ID/status/TRUE_FALSE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccountGroups(callback)</td>
    <td style="padding:15px">List Account Groups</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountAccountGroup(body, callback)</td>
    <td style="padding:15px">Account Account Group</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccountGroupNames(callback)</td>
    <td style="padding:15px">List Account Group Names</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccountGroupNamesbyCloudType(callback)</td>
    <td style="padding:15px">List Account Group Names by Cloud Type</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group/name/CLOUD_TYPE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountGroupInfo(callback)</td>
    <td style="padding:15px">Account Group Info</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group/ACCOUNT_GROUP_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccountGroup(body, callback)</td>
    <td style="padding:15px">Update Account Group</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group/ACCOUNT_GROUP_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountGroup(callback)</td>
    <td style="padding:15px">Delete Account Group</td>
    <td style="padding:15px">{base_path}/{version}/cloud/group/ACCOUNT_GROUP_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyFilters(callback)</td>
    <td style="padding:15px">List Policy Filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/policy/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyFilterAutocompleteSuggestions(body, callback)</td>
    <td style="padding:15px">List Policy Filter Autocomplete Suggestions</td>
    <td style="padding:15px">{base_path}/{version}/filter/policy/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicies(policyName, policySeverity, policyLabel, policyType, policyComplianceStandard, policyComplianceRequirement, policyComplianceSection, policyEnabled, policyPolicyMode, policyRemediable, cloudType, callback)</td>
    <td style="padding:15px">List Policies</td>
    <td style="padding:15px">{base_path}/{version}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">incompleteCOMPLEXAddPolicy(body, callback)</td>
    <td style="padding:15px">(incomplete) COMPLEX - Add Policy</td>
    <td style="padding:15px">{base_path}/{version}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">incompleteCOMPLEXUpdatePolicy(body, callback)</td>
    <td style="padding:15px">(incomplete) COMPLEX - Update Policy</td>
    <td style="padding:15px">{base_path}/{version}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPoliciesV2(policySeverity, policyType, callback)</td>
    <td style="padding:15px">List Policies V2</td>
    <td style="padding:15px">{base_path}/{version}/v2/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyInfo(callback)</td>
    <td style="padding:15px">Policy Info</td>
    <td style="padding:15px">{base_path}/{version}/policy/POLICY_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(callback)</td>
    <td style="padding:15px">Delete Policy</td>
    <td style="padding:15px">{base_path}/{version}/policy/POLICY_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyStatus(callback)</td>
    <td style="padding:15px">Update Policy Status</td>
    <td style="padding:15px">{base_path}/{version}/policy/POLICY_ID/status/TRUE_FALSE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyComplianceStandards(callback)</td>
    <td style="padding:15px">List Policy Compliance Standards</td>
    <td style="padding:15px">{base_path}/{version}/policy/compliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">absoluteTime(callback)</td>
    <td style="padding:15px">Absolute Time</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertFilters(callback)</td>
    <td style="padding:15px">List Alert Filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/alert/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertFiltersCopy(detailed, body, callback)</td>
    <td style="padding:15px">List Alert Filters Copy</td>
    <td style="padding:15px">{base_path}/{version}/alert/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertsV2(timeType, timeAmount, timeUnit, detailed, callback)</td>
    <td style="padding:15px">List Alerts V2</td>
    <td style="padding:15px">{base_path}/{version}/v2/alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alertInfo(callback)</td>
    <td style="padding:15px">Alert Info</td>
    <td style="padding:15px">{base_path}/{version}/alert/P-117144?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertRemediationCommands(body, callback)</td>
    <td style="padding:15px">List Alert Remediation Commands</td>
    <td style="padding:15px">{base_path}/{version}/remediation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInventoryFiltersV2(callback)</td>
    <td style="padding:15px">List Inventory Filters V2</td>
    <td style="padding:15px">{base_path}/{version}/filter/v2/inventory/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInventoryDashboardFilterAutocompleteSuggestionsV2(body, callback)</td>
    <td style="padding:15px">List Inventory Dashboard Filter Autocomplete Suggestions V2</td>
    <td style="padding:15px">{base_path}/{version}/filter/v2/inventory/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSavedAssetInventoryFilters(callback)</td>
    <td style="padding:15px">List Saved Asset Inventory Filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSavedAssetInventoryFiltersCopy(callback)</td>
    <td style="padding:15px">List Saved Asset Inventory Filters Copy</td>
    <td style="padding:15px">{base_path}/{version}/filter/inventory/SAVED_INVENTORY_NAME?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assetInventoryViewV2(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Asset Inventory View V2</td>
    <td style="padding:15px">{base_path}/{version}/v2/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assetInventoryTrendViewV2(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Asset Inventory Trend View V2</td>
    <td style="padding:15px">{base_path}/{version}/v2/inventory/trend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatisticsBreakdown(toNow, cloudAccount, cloudType, callback)</td>
    <td style="padding:15px">Get Compliance Statistics Breakdown</td>
    <td style="padding:15px">{base_path}/{version}/compliance/posture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatisticsforComplianceStandardID(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Get Compliance Statistics for Compliance Standard ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/posture/fd9e3d3a-29a0-4ddc-b058-6f7eabd7d85a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatisticsforRequirementID(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Get Compliance Statistics for Requirement ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/posture/fd9e3d3a-29a0-4ddc-b058-6f7eabd7d85a/a467eb11-b55b-4484-83ba-f55a7ef4358b?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatisticsOverTime(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Get Compliance Statistics Over Time</td>
    <td style="padding:15px">{base_path}/{version}/compliance/posture/trend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatisticsOverTimeforComplianceStandardID(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Get Compliance Statistics Over Time for Compliance Standard ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/posture/trend/fd9e3d3a-29a0-4ddc-b058-6f7eabd7d85a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceStatisticsOverTimeforRequirementID(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Get Compliance Statistics Over Time for Requirement ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/posture/trend/fd9e3d3a-29a0-4ddc-b058-6f7eabd7d85a/a467eb11-b55b-4484-83ba-f55a7ef4358b?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceOverviewFiltersandOptions(callback)</td>
    <td style="padding:15px">Get Compliance Overview Filters and Options</td>
    <td style="padding:15px">{base_path}/{version}/filter/compliance/posture/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">complianceResourceStatisticsBreakdown(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Compliance Resource Statistics Breakdown</td>
    <td style="padding:15px">{base_path}/{version}/compliance/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">overallComplianceResourceStatisticsOverTime(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Overall Compliance Resource Statistics Over Time</td>
    <td style="padding:15px">{base_path}/{version}/compliance/dashboard/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceDashboardFilters(callback)</td>
    <td style="padding:15px">Get Compliance Dashboard Filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/compliance/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceDashboardFiltersCopy(callback)</td>
    <td style="padding:15px">Get Compliance Dashboard Filters Copy</td>
    <td style="padding:15px">{base_path}/{version}/filter/compliance/suggest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listComplianceStandards(callback)</td>
    <td style="padding:15px">List Compliance Standards</td>
    <td style="padding:15px">{base_path}/{version}/compliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listComplianceStandardsbyID(callback)</td>
    <td style="padding:15px">List Compliance Standards by ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/COMPLIANCE_ID_HERE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listComplianceRequirementsbyID(callback)</td>
    <td style="padding:15px">List Compliance Requirements by ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/COMPLIANCE_ID_HERE/requirement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComplianceRequirementbyID(callback)</td>
    <td style="padding:15px">Get Compliance Requirement by ID</td>
    <td style="padding:15px">{base_path}/{version}/compliance/requirement/REQUIREMENT_ID_HERE?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listComplianceRequirementSections(callback)</td>
    <td style="padding:15px">List Compliance Requirement Sections</td>
    <td style="padding:15px">{base_path}/{version}/compliance/REQUIREMENT_ID_HERE/section?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">prismaCloudAuditLogs(timeType, timeAmount, timeUnit, callback)</td>
    <td style="padding:15px">Prisma Cloud Audit Logs</td>
    <td style="padding:15px">{base_path}/{version}/audit/redlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licensingUsageCountforCompute(body, callback)</td>
    <td style="padding:15px">Licensing Usage Count for Compute</td>
    <td style="padding:15px">{base_path}/{version}/usage/compute/twistlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">httpsApiPrismacloudIoSearchConfig(body, callback)</td>
    <td style="padding:15px">https://api.prismacloud.io/search/config</td>
    <td style="padding:15px">{base_path}/{version}/search/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertRulesV2(callback)</td>
    <td style="padding:15px">List Alert Rules V2</td>
    <td style="padding:15px">{base_path}/{version}/v2/alert/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testIntegrationIncomplete(body, callback)</td>
    <td style="padding:15px">Test Integration (incomplete)</td>
    <td style="padding:15px">{base_path}/{version}/integration/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMetadataforAllIntegrations(type, callback)</td>
    <td style="padding:15px">List Metadata for All Integrations</td>
    <td style="padding:15px">{base_path}/{version}/integration/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllIntegrationTypes(callback)</td>
    <td style="padding:15px">List All Integration Types</td>
    <td style="padding:15px">{base_path}/{version}/integration/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllIntegrations(callback)</td>
    <td style="padding:15px">List All Integrations</td>
    <td style="padding:15px">{base_path}/{version}/integration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationsByID(callback)</td>
    <td style="padding:15px">Get Integrations By ID</td>
    <td style="padding:15px">{base_path}/{version}/integration/INTEG_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegration(body, callback)</td>
    <td style="padding:15px">Update Integration</td>
    <td style="padding:15px">{base_path}/{version}/integration/INTEG_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegration(callback)</td>
    <td style="padding:15px">Delete Integration</td>
    <td style="padding:15px">{base_path}/{version}/integration/INTEG_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseSettings(callback)</td>
    <td style="padding:15px">Enterprise Settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/enterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enterpriseSettingsCopy(body, callback)</td>
    <td style="padding:15px">Enterprise Settings Copy</td>
    <td style="padding:15px">{base_path}/{version}/settings/enterprise?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addScanAsset(body, callback)</td>
    <td style="padding:15px">Add Scan Asset</td>
    <td style="padding:15px">{base_path}/{version}/iac/v2/scans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">initiateScanJob(body, callback)</td>
    <td style="padding:15px">Initiate Scan Job</td>
    <td style="padding:15px">{base_path}/{version}/iac/v2/scans/SCAN_ID?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScanJobStatus(callback)</td>
    <td style="padding:15px">Get Scan Job Status</td>
    <td style="padding:15px">{base_path}/{version}/iac/v2/scans/SCAN_ID/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScanResultDetails(callback)</td>
    <td style="padding:15px">Get Scan Result Details</td>
    <td style="padding:15px">{base_path}/{version}/iac/v2/scans/SCAN_ID/results?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportAppCustomers(callback)</td>
    <td style="padding:15px">Get Support App Customers</td>
    <td style="padding:15px">{base_path}/{version}/_support/customer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
