
## 0.2.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-paloalto_prismacloud!1

---

## 0.1.1 [01-18-2022]

- Initial Commit

See commit 692ec7c

---
